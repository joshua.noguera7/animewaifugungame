﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class movement : MonoBehaviour
{
    public float speed = 0.1f;
    public float jumpVel = 5f;
    public bool facingRight = false;
    public int health = 1;
    public string rightKey = "right";
    public string leftKey = "left";
    public string upKey = "up";
    public string shootKey = "space";
    private int shoot = 0;
    public int[] ammo = new int[6];
    private int i = 0;
    private int revolve = 0;

    public bool isGrounded;
    public Rigidbody2D bulletPrefab;
    private Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        System.Random rand = new System.Random();
        rb = GetComponent<Rigidbody2D>();
        for (i = 0; i < 6; i++) 
        { if (rand.NextDouble() > 0.3) { ammo[i] = 1; }
            else
            { ammo[i] = 0; }
        }
    }

    // Update is called once per frame
    void Update(){
    }
    
    void OnTriggerEnter2D(Collider2D collider){
        if (collider.gameObject.GetComponent<bulletmovement>().owner != gameObject) {
            health--;
            if (health <= 0) {
                Destroy(gameObject);
                Application.Quit();
            }
            Destroy(collider.gameObject);
        }
        
    }
    
    void OnTriggerStay2D(Collider2D collider){
        if (collider.gameObject.CompareTag("Ground")) {
            isGrounded = true;
         //   Debug.Log("Ground");
        }
        else if (collider.gameObject.CompareTag("ammo"))
        {
            for (i = 0; i < 6; i++)
            {
                ammo[i] = 1;
            }
        }
        else if (collider.gameObject.CompareTag("death"))
        {
            health = 0;
            Destroy(gameObject);
            Application.Quit();
        }
        //Debug.Log("NoGround");
    }
    
    void FixedUpdate() {
        if (Input.GetKey(rightKey)) {
            transform.position += new Vector3(speed,0,0);
            if (!facingRight) {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                facingRight = true;
            }
        }
        
        if (Input.GetKey(leftKey)) {
            transform.position -= new Vector3(speed,0,0);
            if (facingRight) {
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
                facingRight = false;
            }
        }
        
        if (Input.GetKey(upKey) && isGrounded) {
            rb.velocity = new Vector3(0,jumpVel,0);
            isGrounded = false;
        }
        shoot = shoot+1;
        if (Input.GetKey(shootKey) && shoot>0 && ammo[revolve]==1) {
            ammo[revolve] = 0; revolve = revolve + 1;
            if (revolve == 6) { revolve = 0; }
            shoot = -15;
            Rigidbody2D p = Instantiate(bulletPrefab, new Vector3(0,0,0), Quaternion.identity);
            p.GetComponent<bulletmovement>().owner = gameObject;
            
            if (facingRight){
                p.transform.position = transform.position;
                p.velocity = new Vector3(+17f, 0, 0);
            } else {
                p.transform.position = transform.position;
                p.velocity = new Vector3(-17f, 0, 0);
            }
        }
        if (Input.GetKey(shootKey) && shoot > 0 && ammo[revolve] == 0)
        {
            ammo[revolve] = 0; revolve = revolve + 1;
            if (revolve == 6) { revolve = 0; }
            shoot = -10;
        }
        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }
    
}
