﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class healthtracker : MonoBehaviour
{
    public GameObject trackedPlayer;
    public int scaleInitial = 20;
    public bool animated = true;
    private Transform t;
    
    // Start is called before the first frame update
    void Start()
    {
        t = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (animated) {
            if (trackedPlayer) {
                Vector3 scale = t.localScale;
                int health = trackedPlayer.GetComponent<movement>().health;
                scale.x = scaleInitial * health / 10;
                transform.localScale = scale;
            } else {
                Destroy(gameObject);
                animated = false;
                Application.Quit();
            }
        }
    }
}
